package personal.fengyuxx.flutter.default_http_header_plugin;

import android.content.Context;

import java.util.HashMap;

import androidx.annotation.NonNull;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/** DefaultHttpHeaderPlugin */
public class DefaultHttpHeaderPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
  public static Context context;

  private MethodChannel channel;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "default_http_header_plugin");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel.setMethodCallHandler(null);
  }

  @Override
  public void onMethodCall(MethodCall call, Result result) {
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    }else if(call.method.equals("getAllDefaultHeaders")){
      HashMap<String,Object> map=new HashMap<>();
      map.put("user-agent",System.getProperty("http.agent"));
      result.success(map);
    } else if(call.method.equals("getAcceptLanguage")){
      result.success(context.getResources().getConfiguration().locale.getLanguage());
    } else if(call.method.equals("getUserAgent")){
      result.success(System.getProperty("http.agent"));
    } else {
      result.notImplemented();
    }
  }

  @Override
  public void onAttachedToActivity(@NonNull ActivityPluginBinding activityPluginBinding) {
    context = activityPluginBinding.getActivity().getBaseContext();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
    context = null;
  }

  @Override
  public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding activityPluginBinding) {
    context = activityPluginBinding.getActivity().getBaseContext();
  }

  @Override
  public void onDetachedFromActivity() {
    context = null;
  }
}
